class_name ProfileController
extends Reference


var profile: ProfileModel


func _init(p_profile: ProfileModel) -> void:
	profile = p_profile

# getter and setter
func get_username() -> String:
	return profile.username


func set_username(p_new_username) -> void:
	profile.username = p_new_username


func get_email() -> String:
	return profile.email


func set_email(p_new_email: String) -> void:
	profile.email = p_new_email


func get_age() -> int:
	return profile.age


func set_age(p_new_age: int) -> void:
	profile.age = p_new_age
