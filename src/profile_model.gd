class_name ProfileModel
extends Reference

# public variables ?
# => accessible by other script.

# but some model contains private variables as well.

# these are public variables
var username: String
var email: String
var age: int


# initalize ?
# initalize ?
func _init(
	p_username: String,
	p_email: String,
	p_age: int
) -> void:
	username = p_username
	email = p_email
	age = p_age
