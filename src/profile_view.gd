extends VBoxContainer


var profile_controller: ProfileController


func _ready():
	var m_profile = ProfileModel.new("John Doe", "john@callbreak.com", 22) 
	profile_controller = ProfileController.new(m_profile)


func _on_UserName_pressed():
	var m_new_username: String = "Ram Bahadur Thapa"
	profile_controller.set_username(m_new_username)
	
	# update in button1 through controller
	$UserName.text = profile_controller.get_username()


func _on_Email_pressed():
	var m_new_email: String = "ram@callbreak.com"
	profile_controller.set_email(m_new_email)
	
	# update in button1 through controller
	$Email.text = profile_controller.get_email()


func _on_Age_pressed():
	var m_new_age: int = 22
	profile_controller.set_age(m_new_age)
	
	$Age.text = str(profile_controller.get_age())


# 0705
